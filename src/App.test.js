import { render, screen } from '@testing-library/react';
import App from './App';

const calculate = (a, b, operation) => {
        if (operation !== 'Select operation') {
            switch (operation) {
                case 'ADDITION':
                    return parseFloat(a) + parseFloat(b);
                case 'DIVISION':
                    return parseFloat(a) / parseFloat(b);
                case 'DIVISION_REMAINDER':
                    return parseFloat(a) % parseFloat(b);
                case 'HIGHEST_PRIME':
                    const array = [a, b]
                    const lowerNumber = Math.min(...array)
                    const higherNumber = Math.max(...array)
                    const resultArray = []
                    if (lowerNumber % 1 === 0 && higherNumber % 1 === 0 && lowerNumber >= 0 && higherNumber >= 0) {
                        for (let i = lowerNumber; i <= higherNumber; i++) {
                            let flag = 0;
                            // looping through 2 to user input number
                            for (let j = 2; j < i; j++) {
                                if (i % j === 0) {
                                    flag = 1;
                                    break;
                                }
                            }
                            // if number greater than 1 and not divisible by other numbers
                            if (i > 1 && flag === 0) {
                                resultArray.push(i)
                            }
                        }
                        return Math.max(...resultArray)
                    } else {
                        return "Prime numbers are whole positive numbers!"
                    }
                    
                default:
                    return null; 
            }
        } else {
            return "Please select operation!"
        }

}


test('renders Calculator header', () => {
  render(<App />);
  const linkElement = screen.getByText(/Calculator/i);
  expect(linkElement).toBeInTheDocument();
});


test('calculate addition 45 + 56 to be 101', () => {
  expect(calculate(45, 56, "ADDITION")).toBe(101)
});

test('calculate highest prime number between 0.1 and 100 to be error - Prime numbers are whole positive numbers!', () => {
  expect(calculate(0.1, 100, "HIGHEST_PRIME")).toBe("Prime numbers are whole positive numbers!")
});

test('calculate highest prime number between 1 and 1000 to be 997', () => {
  expect(calculate(1, 1000, "HIGHEST_PRIME")).toBe(997)
});

test('calculate division 123 / 2.2 to be 55.90909090909091', () => {
  expect(calculate(123, 2.2, "DIVISION")).toBe(55.90909090909091)
});

test('calculate division remainder of 123 / 4', () => {
  expect(calculate(123, 4, "DIVISION_REMAINDER")).toBe(3)
});
