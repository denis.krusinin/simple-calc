import React, { useState } from 'react';
import NumberInput from './NumberInput';
import Select from 'react-select';
import Result from './Result';
import HistoryTable from './HistoryTable'

const CalculatorForm = (props) => {
    const [selectedOptionState, setSelectedOptionState] = useState({
        selectedOptionState: {
            value: 'Select operation',
            label: 'Select operation',
        },
    });

    const [resultState, setResultState] = useState(null);
    const options = [
        { value: 'ADDITION', label: 'Addition' },
        { value: 'DIVISION', label: 'Division' },
        { value: 'DIVISION_REMAINDER', label: 'Remainder of a division' },
        { value: 'HIGHEST_PRIME', label: 'Highest prime number' },
    ];

    let calculate = () => {
        const { x, y } = props;
        if (selectedOptionState.selectedOptionState.value !== 'Select operation') {
            switch (selectedOptionState.selectedOptionState.value) {
                case 'ADDITION':
                    return parseFloat(x) + parseFloat(y);
                case 'DIVISION':
                    return parseFloat(x) / parseFloat(y);
                case 'DIVISION_REMAINDER':
                    return parseFloat(x) % parseFloat(y);
                case 'HIGHEST_PRIME':
                    const array = [x, y]
                    const lowerNumber = Math.min(...array)
                    const higherNumber = Math.max(...array)
                    const resultArray = []
                    if (lowerNumber % 1 === 0 && higherNumber % 1 === 0 && lowerNumber >= 0 && higherNumber >= 0) {
                        for (let i = lowerNumber; i <= higherNumber; i++) {
                            let flag = 0;
                            // looping through 2 to user input number
                            for (let j = 2; j < i; j++) {
                                if (i % j === 0) {
                                    flag = 1;
                                    break;
                                }
                            }
                            // if number greater than 1 and not divisible by other numbers
                            if (i > 1 && flag === 0) {
                                resultArray.push(i)
                            }
                        }
                        return Math.max(...resultArray)
                    } else {
                        return "Prime numbers are whole positive numbers!"
                    }
                    
                default:
                    return null; 
            }
        } else {
            selectedOptionState.selectedOptionState.value = "Please select operation!"
            return "Please select operation!"
        }
       
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const result = calculate();
       
        setResultState(result);
        var historyArray = JSON.parse(localStorage.getItem("history") || '[]')
        var history = {
            date: new Date(),
            firstNum: props.x,
            secondNum: props.y,
            operation: selectedOptionState.selectedOptionState.value,
            result: result
        }
        if (result !== "Please select operation!" && result !== "Prime numbers are whole positive numbers!") {
            historyArray.push(history)
            localStorage.setItem("history", JSON.stringify(historyArray)) 
        }
      
        };
    const handleChange = (selectedOptionState) => {
        setSelectedOptionState({ selectedOptionState });
    };

    return (
        <div>
            <form className="CalculatorForm" onSubmit={handleSubmit}>
                <NumberInput
                    OnChange={props.xOnChange}
                    name="x"
                    label="First number:"
                    value={props.x}
                />
                <NumberInput
                    OnChange={props.yOnChange}
                    name="y"
                    label="Second number:"
                    value={props.y}
                />
                <Select
                    onChange={handleChange}
                    value={selectedOptionState.selectedOption}
                    options={options}
                />
                <input value="Count" type="submit" />
            </form>
            <Result value={resultState} />
            <HistoryTable  />
        </div>
    );
};

export default CalculatorForm;
