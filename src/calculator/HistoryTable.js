import React from 'react';
import moment from 'moment'

const HistoryTable = () => {
    const history = localStorage.getItem("history")
    let historyToDisplay = JSON.parse(history)
   if (history && history.length) 
        return <div>
          <table className='HistoryTable'>
                <tr>
                    <th>Date</th>
                    <th>First number</th>
                    <th>Second number</th>
                    <th>Operation</th>
                    <th>Result</th>
                </tr>
                {historyToDisplay.map((val, key) => {
                    return (
                        <tr key={key}>
                            <td>{moment(val.date).format("MMMM Do YYYY, h:mm:ss a")}</td>
                            <td>{val.firstNum}</td>
                            <td>{val.secondNum}</td>
                            <td>{val.operation}</td>
                            <td>{val.result}</td>
                        </tr>
                    )
                })}
            </table></div>
    
};

export default HistoryTable;

