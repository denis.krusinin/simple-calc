import React from 'react';

const Result = (props) => {
    const result = props.value;
    if ((result || result === 0) && typeof result === 'number')
        return <div className="Result">Result: {result}</div>;
    else if (typeof result !== 'number') 
        return <div className="Result Error">{result}</div>;
    else
        return null;
};

export default Result;
